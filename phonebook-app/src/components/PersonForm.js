import React from 'react';

const PersonForm = (props) => {
    const {handleSubmit, handleChangePersonName, newName, handleChangePersonNumber, newNumber, handleClick} = props
    return (
        <form onSubmit={handleSubmit}>
            <div>
                name: <input onChange={handleChangePersonName} type="text" value={newName}/>
            </div>
            <div>
                number: <input onChange={handleChangePersonNumber} type="text" value={newNumber} pattern="[0-9]*"/>
            </div>
            <div>
                <button onClick={handleClick} type="submit">add</button>
            </div>
        </form>
    )
}

export default PersonForm
