import React from 'react';
import {v4 as idd} from "uuid";

const Persons = (props) => {
    const {filter, newName} = props
    return (
        <div>
            <ul>
                {
                    filter.map((item) => {
                        if (item.name === newName) {
                            alert(`${newName} is already added to phonebook`)
                        }
                        return (
                            <li key={idd()}>{item.name} - {item.number}</li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default Persons
