import React from 'react';
import Content from "../components/content/Content";
import Header from "../components/Header";

const Course = props => {
    const {course} = props

    return (
        <div>
            {course.map((key) => {
                return (
                    <ul>
                       <li><Header courseName={key.name}/></li>
                        {key.parts.map((item) => {
                            return (
                                <Content key={item.id} part={item.name} exercises={item.exercises}/>
                            )
                        })}
                        <p>Number of exercises: <b>{key.parts.reduce((prev, next) => {
                            return (
                                prev + next.exercises
                            )
                        }, 0)}</b></p>
                    </ul>
                )
            })}
        </div>
    );
};

export default Course;
