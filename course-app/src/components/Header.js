import React from 'react';

const Header = props => {
    const {courseName} = props
    return (
        <div>
            <h2>{courseName}</h2>
        </div>
    );
}

export default Header;
