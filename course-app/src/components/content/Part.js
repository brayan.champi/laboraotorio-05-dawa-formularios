import React from 'react';

const Part = props => {
    const {key, part, exercises} = props
    return (
        <div>
            <p key={key}>{part} {exercises}</p>
        </div>
    );
};

export default Part;
