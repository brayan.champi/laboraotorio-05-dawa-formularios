import React from 'react';
import Part from "./Part";

const Content = props => {
    const {keyP, part, exercises} = props
    return (
        <div>
            <Part key={keyP} part={part} exercises={exercises}/>
        </div>
    );
};

export default Content;
