import React from 'react'
import ReactDOM from 'react-dom'
import Course from "./pages/Course";
import {v4 as idd} from 'uuid';

const App = () => {
    const course = [
        {
            name: 'Half Stack application development',
            id: idd(),
            parts: [
                {
                    name: 'Fundamentals of React',
                    exercises: 10,
                    id: idd()
                },
                {
                    name: 'Using props to pass data',
                    exercises: 7,
                    id: idd()
                },
                {
                    name: 'State of a component',
                    exercises: 14,
                    id: idd()
                },
                {
                    name: 'Redux',
                    exercises: 11,
                    id: idd()
                }
            ]
        },
        {
            name: 'Node.js',
            id: idd(),
            parts: [
                {
                    name: 'Routing',
                    exercises: 3,
                    id: idd()
                },
                {
                    name: 'Middlewares',
                    exercises: 7,
                    id: idd()
                }
            ]
        }
    ]

    return <Course course={course} />
}

ReactDOM.render(<App />, document.getElementById('root'))
